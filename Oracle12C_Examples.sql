--ANSI 99
/*SELECT e.employee_id, e.first_name, e.last_name, e.department_id, d.department_name
FROM employees e
INNER JOIN departments d
ON (e.department_id = d.department_id)
WHERE e.commission_pct IS NULL;

--Equijoin
SELECT e.employee_id, e.first_name, e.last_name, 
    e.department_id, d.department_name
FROM employees e, departments d
WHERE e.department_id = d.department_id
AND e.commission_pct IS NULL;

--LEFT, RIGTH y FULL OUTER JOIN
SELECT e.employee_id, e.first_name, e.last_name,
    jefe.employee_id, jefe.first_name, jefe.last_name 
FROM employees e 
LEFT OUTER JOIN employees jefe
ON (e.manager_id = jefe.employee_id);

SELECT e.employee_id, e.first_name, e.last_name,
    jefe.employee_id, jefe.first_name, jefe.last_name 
FROM employees e, employees jefe
WHERE e.manager_id = jefe.employee_id (+);

SELECT *
FROM departments d
where d.manager_id IS NULL;

SELECT *
FROM employees e 
RIGHT OUTER JOIN departments d
ON (d.manager_id = e.employee_id);

SELECT *
FROM employees e, departments d
WHERE e.employee_id (+) = d.manager_id;

SELECT *
FROM employees e 
FULL OUTER JOIN departments d
ON (d.manager_id = e.employee_id);

--NO VALIDO
SELECT *
FROM employees e, departments d
WHERE e.employee_id (+) = d.manager_id (+);

SELECT *
FROM employees e, departments d
WHERE e.employee_id (+) = d.manager_id
UNION
SELECT *
FROM employees e, departments d
WHERE e.employee_id = d.manager_id (+);*/

DECLARE
    v_numero NUMBER(5,2);
    v_cadena_1 VARCHAR2(100) := 'CADENA';
    v_cadena_2 VARCHAR2(100) DEFAULT 'CADENA 2';
    
    c_constante CONSTANT NUMBER := 5.2;
    
    v_salary_1 NUMBER(8,2);
    v_salary employees.salary%TYPE; 
    
    v_flag BOOLEAN := TRUE;
    v_flag BOOLEAN := NULL;
    
    v_conteo NUMBER := 0;
    v_seq NUMBER := 0;
    
    v_fecha DATE := SYSDATE;
BEGIN
    /*DBMS_OUTPUT.PUT_LINE('Valor: ' || v_cadena_1);
    
    v_conteo := LENGTH(v_cadena_2);
    
    DBMS_OUTPUT.PUT_LINE('N�mero de caracteres: ' || v_conteo);
    
    v_seq := DEPARTMENTS_SEQ.NEXTVAL;
    
    DBMS_OUTPUT.PUT_LINE('Consecutivo secuencia: ' || v_seq);*/
    
    DBMS_OUTPUT.PUT_LINE('Fecha actual: ' || TO_CHAR(v_fecha, 'DD/MM/YYYY HH24:MI:SS'));
END;

BEGIN <<outer>>
    DECLARE
        v_outer NUMBER := 25;
    BEGIN
        DECLARE
            v_inner NUMBER := 45;
            v_outer NUMBER := 65;
        BEGIN
            DBMS_OUTPUT.PUT_LINE('v_outer: ' || v_outer);
            DBMS_OUTPUT.PUT_LINE('v_outer externa: ' || outer.v_outer);
            DBMS_OUTPUT.PUT_LINE('v_inner: ' || v_inner);
        END;
        
        DBMS_OUTPUT.PUT_LINE('v_outer: ' || v_outer);
        --NO ES VISIBLE
        --DBMS_OUTPUT.PUT_LINE('v_inner: ' || v_inner);
    END;
END outer;

DECLARE
    v_region_id regions.region_id%TYPE;
    v_region_name regions.region_name%TYPE;
    
    v_average_salary NUMBER := 0;
BEGIN

    SELECT r.region_id, r.region_name
    INTO v_region_id, v_region_name
    FROM regions r
    WHERE r.region_id = 1;
    
    DBMS_OUTPUT.PUT_LINE('Regi�n: ' || v_region_id || ' - ' || v_region_name);
    
    SELECT AVG(e.salary)
    INTO v_average_salary
    FROM employees e;
    
    DBMS_OUTPUT.PUT_LINE('El promedio salarial es: ' || v_average_salary);
END;

DECLARE
    v_region_id regions.region_id%TYPE;
    v_region_name regions.region_name%TYPE;
    
    v_average_salary NUMBER := 0;
BEGIN

    SELECT r.region_id, r.region_name
    INTO v_region_id, v_region_name
    FROM regions r
    WHERE r.region_id = 1;
    
    DBMS_OUTPUT.PUT_LINE('Regi�n: ' || v_region_id || ' - ' || v_region_name);
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        DBMS_OUTPUT.PUT_LINE('La regi�n no existe.');
    WHEN TOO_MANY_ROWS THEN
        DBMS_OUTPUT.PUT_LINE('La consulta retorna muchas regiones.');        
END;

DECLARE
    v_row employees%ROWTYPE;
BEGIN

    SELECT *
    INTO v_row
    FROM employees e
    WHERE e.employee_id = 198;
    
    DBMS_OUTPUT.PUT_LINE('Employee: ' || v_row.EMPLOYEE_ID || ' - ' || v_row.FIRST_NAME || ' ' || v_row.LAST_NAME);
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        DBMS_OUTPUT.PUT_LINE('La regi�n no existe.');
    WHEN TOO_MANY_ROWS THEN
        DBMS_OUTPUT.PUT_LINE('La consulta retorna muchas regiones.');        
END;

DECLARE
    v_number_rows NUMBER;
BEGIN
    --INSERT INTO regions (region_id, region_name)
    --VALUES (5, 'Oceania');
    
    --UPDATE regions r
    --SET r.region_name = 'OCEANIA'
    --WHERE r.region_id = 5;
    
    DELETE FROM regions r
    WHERE r.region_id >= 5;
    
    v_number_rows := SQL%ROWCOUNT;
   
    DBMS_OUTPUT.PUT_LINE('Cantidad de registros procesados: ' || v_number_rows);    
END;

DECLARE
    v_number_rows NUMBER;
BEGIN
    INSERT INTO regions (region_id, region_name)
    VALUES (5, 'Oceania');
    
    v_number_rows := SQL%ROWCOUNT;
    
    COMMIT;
   
    DBMS_OUTPUT.PUT_LINE('Cantidad de registros procesados: ' || v_number_rows);  
    
EXCEPTION
    WHEN OTHERS THEN
        ROLLBACK;
END;

SELECT *
FROM regions;

INSERT INTO regions (region_id, region_name)
VALUES (6, 'Africa Norte');

INSERT INTO regions (region_id, region_name)
VALUES (7, 'Africa Sur');

//1. Consultar la informaci�n del empleado con ID VARIABLE y vamos a imprimir
// Nombre concatenado
// Salario
// Comisi�n -> NULA, Cero
// Nombre del depto
// Nombre del cargo
// Fecha de ingreso DD/MM/YYYY
//Si no existe, "El empleado no existe"
//Retorna m�s de un empleado "Consulta invalida"

DECLARE
    v_number NUMBER := 8000;
BEGIN

    /*IF (mod(v_number, 2) = 0) THEN
        DBMS_OUTPUT.PUT_LINE('El numero: ' || v_number || ' es par');  
    END IF;*/
    
    /*IF (mod(v_number, 2) = 0) THEN
        DBMS_OUTPUT.PUT_LINE('El numero: ' || v_number || ' es par');  
    ELSE
        DBMS_OUTPUT.PUT_LINE('El numero: ' || v_number || ' es impar');  
    END IF;*/
    
    /*IF (v_number < 10) THEN
        DBMS_OUTPUT.PUT_LINE('El numero: ' || v_number || ' es de un digito');  
    ELSE
        IF (v_number >= 10 AND v_number < 100) THEN
            DBMS_OUTPUT.PUT_LINE('El numero: ' || v_number || ' es de dos digitos');  
        ELSE
            DBMS_OUTPUT.PUT_LINE('El numero: ' || v_number || ' es de m�s de dos digitos');  
        END IF;
    END IF;*/
    
    IF (v_number < 10) THEN
        DBMS_OUTPUT.PUT_LINE('El numero: ' || v_number || ' es de un digito');  
    ELSIF(v_number >= 10 AND v_number < 100) THEN
        DBMS_OUTPUT.PUT_LINE('El numero: ' || v_number || ' es de dos digitos'); 
    ELSIF(v_number BETWEEN 100 AND 999) THEN
        DBMS_OUTPUT.PUT_LINE('El numero: ' || v_number || ' es de tres digitos');  
    ELSE
        DBMS_OUTPUT.PUT_LINE('El numero: ' || v_number || ' es de m�s de tres digitos');  
    END IF;
END;

DECLARE
    v_name VARCHAR2(100) := 'Oscar';
    v_msg VARCHAR2(2000);
BEGIN

    v_msg := 
        CASE SUBSTR(v_name, 1, 1)
            WHEN 'A' THEN 'Inicia por A'
            WHEN 'E' THEN 'Inicia por E'
            WHEN 'I' THEN 'Inicia por I'
            ELSE 'No empieza por AEI'
        END;
        
    DBMS_OUTPUT.PUT_LINE(v_msg); 
END;

DECLARE
    v_number NUMBER := 5;
    v_msg VARCHAR2(2000);
BEGIN

    v_msg := 
        CASE 
            WHEN v_number < 10
                THEN 'Es de un digito'
            WHEN v_number >= 10 AND v_number < 100 
                THEN 'Es de dos digitos'
            ELSE 'Es de m�s de dos digitos'
        END;
        
    DBMS_OUTPUT.PUT_LINE(v_msg); 
END;

DECLARE
    v_number NUMBER := 5;
BEGIN
    CASE 
        WHEN v_number < 10 THEN
            DBMS_OUTPUT.PUT_LINE('Es de un digito'); 
        WHEN v_number >= 10 AND v_number < 100 THEN
            DBMS_OUTPUT.PUT_LINE('Es de dos digitos');
        ELSE 
            DBMS_OUTPUT.PUT_LINE('Es de m�s de dos digitos');
    END CASE;
END;

--Pendiente por revisar
DECLARE
    v_name VARCHAR2(100) := 'Cscar';
BEGIN

    CASE SUBSTR(v_name, 1, 1)
        WHEN 'A' OR
        WHEN 'E' OR
        WHEN 'I' OR
        WHEN 'O' OR
        WHEN 'U' THEN 
            DBMS_OUTPUT.PUT_LINE('Inicia por vocal');
        ELSE 
            DBMS_OUTPUT.PUT_LINE('Inicia por consonante');
    END CASE;    
END;

//1. Consultar la informaci�n del empleado por ID, y retornar nombre completo, salario y comision
// Valor a pagar mensual, salario + (salario * comision). --> IF

//2. Consultar la informaci�n del empleado por ID, y retornar nombre completo, salario
// Si el empleado gana entre 0 y 5000 Salario Bajo
// Si el empleado gana entre 5000 y 10000 Salario Medio
// Si el empleado gana m�s de 10000 Salario Alto  -> CASE (Estructura de control)

DECLARE 
    v_contador NUMBER := 1;
BEGIN
    LOOP
        DBMS_OUTPUT.PUT_LINE(v_contador);
        v_contador := v_contador + 1;
        EXIT WHEN v_contador > 10;
    END LOOP;
END;

DECLARE 
    v_contador NUMBER := 1;
BEGIN
    WHILE (v_contador <= 10) LOOP
        DBMS_OUTPUT.PUT_LINE(v_contador);
        v_contador := v_contador + 1;        
    END LOOP;
END;
   
BEGIN
    FOR v_contador IN 1..10 LOOP
        DBMS_OUTPUT.PUT_LINE(v_contador);        
    END LOOP;
END;

BEGIN
    FOR v_contador IN REVERSE 1..10 LOOP
        DBMS_OUTPUT.PUT_LINE(v_contador);        
    END LOOP;
END;


--Ejercicio LOOP y WHILE
DECLARE
    v_employee employees%ROWTYPE;
    v_suma_salarios NUMBER := 0;
BEGIN

    FOR v_contador IN 100..300 LOOP
    
        BEGIN        
            SELECT *
            INTO v_employee
            FROM employees e
            WHERE e.employee_id = v_contador;
            
            DBMS_OUTPUT.PUT_LINE('Empleado: ' || v_employee.employee_id || ' ' || 
                CONCAT(v_employee.first_name, CONCAT(' ', v_employee.last_name)) || ' Salario: ' || v_employee.salary);
                
            v_suma_salarios := v_suma_salarios + v_employee.salary;
        EXCEPTION 
            WHEN NO_DATA_FOUND THEN
                NULL;
        END;
    
    END LOOP;
    
    DBMS_OUTPUT.PUT_LINE('Suma total: ' || v_suma_salarios);
END;

SELECT *
FROM employees e
order by e.employee_id;


BEGIN
    FOR i IN 1..20 LOOP
        CONTINUE WHEN (MOD(i,2) = 0);
        
        DBMS_OUTPUT.PUT_LINE(i);
    END LOOP;   
END;

-----------------------

CREATE TABLE EMPLOYEES_WITH_COMM
AS SELECT *
FROM EMPLOYEES;

TRUNCATE TABLE EMPLOYEES_WITH_COMM;

SELECT *
FROM EMPLOYEES_WITH_COMM;


DECLARE
    v_employee employees%ROWTYPE;    
BEGIN
    FOR v_contador IN 100..300 LOOP
    
        BEGIN        
            SELECT *
            INTO v_employee
            FROM employees e
            WHERE e.employee_id = v_contador;
            
            IF (v_employee.COMMISSION_PCT IS NOT NULL) THEN
                /*INSERT INTO employees_with_comm (
                    employee_id, first_name, last_name,
                    email, phone_number, hire_date,
                    job_id, salary, commission_pct,
                    manager_id, department_id)
                VALUES (v_employee.employee_id, v_employee.first_name...);*/
                INSERT INTO EMPLOYEES_WITH_COMM VALUES v_employee;
            END IF;
        EXCEPTION 
            WHEN NO_DATA_FOUND THEN
                NULL;
        END;
    
    END LOOP;
    
    COMMIT;
END;


DECLARE
    v_employee EMPLOYEES_WITH_COMM%ROWTYPE;    
BEGIN
    FOR v_contador IN 100..300 LOOP
    
        BEGIN        
            SELECT *
            INTO v_employee
            FROM EMPLOYEES_WITH_COMM e
            WHERE e.employee_id = v_contador;
            
            v_employee.HIRE_DATE := SYSDATE;
            
            UPDATE EMPLOYEES_WITH_COMM
            SET ROW = v_employee
            WHERE EMPLOYEE_ID = v_employee.EMPLOYEE_ID;
        EXCEPTION 
            WHEN NO_DATA_FOUND THEN
                NULL;
        END;
    
    END LOOP;
    
    COMMIT;
END;

DECLARE
    TYPE t_emp_tmp IS RECORD(
        v_full_name VARCHAR2(50),
        v_salary employees.SALARY%TYPE,
        v_commission employees.COMMISSION_PCT%TYPE NOT NULL := 0,
        v_depto employees.DEPARTMENT_ID%TYPE
    );
    
    v_emp t_emp_tmp;
BEGIN

    DBMS_OUTPUT.PUT_LINE('Empleado inicial: ' || v_emp.v_full_name || ' - ' || v_emp.v_commission);

    SELECT CONCAT(e.FIRST_NAME, CONCAT(' ', e.LAST_NAME)),
        e.SALARY, NVL(e.COMMISSION_PCT, 0), e.DEPARTMENT_ID
    INTO v_emp.v_full_name, v_emp.v_salary, v_emp.v_commission,
        v_emp.v_depto
    FROM employees e
    WHERE e.EMPLOYEE_ID = 145;
    
    v_emp.v_commission := v_emp.v_commission + 0.01;
    
    DBMS_OUTPUT.PUT_LINE('Empleado: ' || v_emp.v_full_name || ' - ' || v_emp.v_salary  || ' - '  || v_emp.v_commission);
END;

DECLARE
    TYPE t_emp_tmp IS RECORD(
        v_full_name VARCHAR2(50),
        v_salary employees.SALARY%TYPE,
        v_commission employees.COMMISSION_PCT%TYPE NOT NULL := 0,
        v_depto employees.DEPARTMENT_ID%TYPE
    );
    
    v_emp t_emp_tmp;
BEGIN

    DBMS_OUTPUT.PUT_LINE('Empleado inicial: ' || v_emp.v_full_name || ' - ' || v_emp.v_commission);

    SELECT CONCAT(e.FIRST_NAME, CONCAT(' ', e.LAST_NAME)) AS v_full_name,
        e.SALARY AS v_salary, NVL(e.COMMISSION_PCT, 0) AS v_commission, e.DEPARTMENT_ID AS v_depto
    INTO v_emp
    FROM employees e
    WHERE e.EMPLOYEE_ID = 145;
    
    v_emp.v_commission := v_emp.v_commission + 0.01;
    
    DBMS_OUTPUT.PUT_LINE('Empleado: ' || v_emp.v_full_name || ' - ' || v_emp.v_salary  || ' - '  || v_emp.v_commission);
END;


DECLARE
    TYPE t_emp_name_list IS TABLE OF VARCHAR2(50)
        INDEX BY PLS_INTEGER;
        
    v_emp_list t_emp_name_list;
BEGIN
    v_emp_list(-25) := 'Jose Medina';
    v_emp_list(0) := 'Carlos Ortiz';
    v_emp_list(15) := 'Maria Gomez';
    v_emp_list(-25) := 'Rodrigo Rojas';
    
    DBMS_OUTPUT.PUT_LINE('Cantidad de registros: ' || v_emp_list.COUNT);
    DBMS_OUTPUT.PUT_LINE('Primer elemento: ' || v_emp_list.FIRST || ' - ' || v_emp_list(v_emp_list.FIRST));
    DBMS_OUTPUT.PUT_LINE('Ultimo elemento: ' || v_emp_list.LAST || ' - ' || v_emp_list(v_emp_list.LAST));
    
    v_emp_list.DELETE;
    
    DBMS_OUTPUT.PUT_LINE('Cantidad de registros: ' || v_emp_list.COUNT);
END;

DECLARE
    TYPE t_emp_name_list IS TABLE OF employees.FIRST_NAME%TYPE
        INDEX BY PLS_INTEGER;
        
    v_emp_list t_emp_name_list;
BEGIN
    FOR v_contador IN 100..300 LOOP
        BEGIN        
            SELECT e.FIRST_NAME
            INTO v_emp_list(v_contador)
            FROM EMPLOYEES e
            WHERE e.employee_id = v_contador;
        EXCEPTION 
            WHEN NO_DATA_FOUND THEN
                NULL;
        END;
    END LOOP;
     
    DBMS_OUTPUT.PUT_LINE('Cantidad de registros: ' || v_emp_list.COUNT);
    
    FOR v_id_empl IN v_emp_list.FIRST..v_emp_list.LAST LOOP
        IF (v_emp_list.EXISTS(v_id_empl)) THEN
            DBMS_OUTPUT.PUT_LINE('Empleado con ID: ' || v_id_empl || ' - ' || v_emp_list(v_id_empl));
        ELSE
            DBMS_OUTPUT.PUT_LINE('Empleado con ID: ' || v_id_empl || ' No existe ');
        END IF;
    END LOOP;
    
    v_emp_list.DELETE(v_emp_list.FIRST, v_emp_list.LAST);
    DBMS_OUTPUT.PUT_LINE('Cantidad de registros: ' || v_emp_list.COUNT);
END;

--Crear un TABLE usando ROWTYPE
--Consultar la informaci�n de los empleados y la cargamos en el TABLE usando como clave ID del empleado
--Cargar solo los empleados que no tengan comisi�n 
--Los vamos a imprimir, usando los metodos PRIOR y NEXT
--Mostrar que reemplaza, 0 y negativos
--Mostrar key VARCHAR2

--Tabla Anidada
TYPE t_emp_tmp IS TABLE OF employees.first_name%TYPE;
v_emp_tmp t_emp_tmp;

v_emp_tmp := t_emp_tmp('Carlos', 'Cesar');
v_emp_tmp(1);
v_emp_tmp.count();  --> Cantidad de elemento

--VARRAY
TYPE t_emp_tmp IS VARRAY(4) OF employees.first_name%TYPE;
v_emp_tmp t_emp_tmp;

v_emp_tmp := t_emp_tmp('Carlos', 'Cesar', 'Jose', 'Maria');
v_emp_tmp(1);  //1..4
v_emp_tmp.count();  --> Cantidad de elemento

DECLARE
    TYPE t_emp_tmp IS TABLE OF employees.first_name%TYPE;
    v_emp_tmp t_emp_tmp;
BEGIN
    v_emp_tmp := t_emp_tmp('Carlos', 'Cesar');
    
    FOR v_cont IN 1..v_emp_tmp.COUNT() LOOP
       DBMS_OUTPUT.PUT_LINE('Nombre: ' || v_emp_tmp(v_cont)); 
    END LOOP;
END;

DECLARE
    --Declaraci�n
    CURSOR cur_employees IS 
        SELECT *
        FROM employees e
        WHERE e.COMMISSION_PCT IS NOT NULL;
        
    v_empl employees%ROWTYPE;
    v_cant_reg NUMBER;
BEGIN
    OPEN cur_employees;
    
    LOOP
        FETCH cur_employees INTO v_empl;
        
        EXIT WHEN cur_employees%NOTFOUND;
        --EXIT WHEN cur_employees%ROWCOUNT > 10 OR cur_employees%NOTFOUND;
        
        DBMS_OUTPUT.PUT_LINE('ID: ' || v_empl.EMPLOYEE_ID || ' Nombre: ' || v_empl.FIRST_NAME || ' ' || v_empl.LAST_NAME); 
        
        v_cant_reg := cur_employees%ROWCOUNT;
        
        DBMS_OUTPUT.PUT_LINE('Registros Procesados: ' || v_cant_reg); 
    END LOOP;
    
    CLOSE cur_employees;
END;

DECLARE
    --Declaraci�n
    CURSOR cur_employees IS 
        SELECT e.EMPLOYEE_ID, e.FIRST_NAME, e.LAST_NAME,
            e.SALARY
        FROM employees e
        WHERE e.COMMISSION_PCT IS NOT NULL;
        
    v_empl_id employees.EMPLOYEE_ID%TYPE;
    v_empl_fn employees.FIRST_NAME%TYPE;
    v_empl_ln employees.LAST_NAME%TYPE;
    v_empl_salary employees.SALARY%TYPE;
BEGIN
    OPEN cur_employees;
    
    LOOP
        FETCH cur_employees 
        INTO v_empl_id, v_empl_fn,v_empl_ln, 
            v_empl_salary;
        
        EXIT WHEN cur_employees%NOTFOUND;
        
        DBMS_OUTPUT.PUT_LINE('ID: ' || v_empl_id || ' Nombre: ' || v_empl_fn || ' ' || v_empl_ln ||
             ' Salary: ' || v_empl_salary); 
    END LOOP;
    
    CLOSE cur_employees;
END;

DECLARE
    --Declaraci�n
    CURSOR cur_employees IS 
        SELECT e.EMPLOYEE_ID, e.FIRST_NAME, e.LAST_NAME,
            e.SALARY
        FROM employees e
        WHERE e.COMMISSION_PCT IS NOT NULL;
        
    TYPE t_empl_tmp IS RECORD (
        v_empl_id employees.EMPLOYEE_ID%TYPE,
        v_empl_fn employees.FIRST_NAME%TYPE,
        v_empl_ln employees.LAST_NAME%TYPE,
        v_empl_salary employees.SALARY%TYPE
    );
    
    v_empl_tmp t_empl_tmp;
BEGIN
    OPEN cur_employees;
    
    LOOP
        FETCH cur_employees 
        INTO v_empl_tmp;
        
        EXIT WHEN cur_employees%NOTFOUND;
        
        DBMS_OUTPUT.PUT_LINE('ID: ' || v_empl_tmp.v_empl_id || ' Nombre: ' || v_empl_tmp.v_empl_fn || ' ' || v_empl_tmp.v_empl_ln ||
             ' Salary: ' || v_empl_tmp.v_empl_salary); 
    END LOOP;
    
    CLOSE cur_employees;
END;

DECLARE
    --Declaraci�n
    CURSOR cur_employees IS 
        SELECT *
        FROM employees e
        WHERE e.COMMISSION_PCT IS NOT NULL;
        
    v_empl employees%ROWTYPE;
BEGIN
    OPEN cur_employees;
    
    LOOP
        FETCH cur_employees INTO v_empl;
        
        EXIT WHEN cur_employees%NOTFOUND;
        
        RAISE_APPLICATION_ERROR(-20001, 'Error inducido');
        
        DBMS_OUTPUT.PUT_LINE('ID: ' || v_empl.EMPLOYEE_ID || ' Nombre: ' || v_empl.FIRST_NAME || ' ' || v_empl.LAST_NAME); 
    END LOOP;
    
    CLOSE cur_employees;
EXCEPTION
    WHEN OTHERS THEN
        IF (cur_employees%ISOPEN) THEN 
            DBMS_OUTPUT.PUT_LINE('Cerrando Cursor');
            CLOSE cur_employees;
        END IF;
END;

DECLARE
    --Declaraci�n
    CURSOR cur_employees IS 
        SELECT *
        FROM employees e
        WHERE e.COMMISSION_PCT IS NOT NULL;
        
    --v_empl employees%ROWTYPE;
BEGIN
    FOR v_empl IN cur_employees LOOP
        DBMS_OUTPUT.PUT_LINE('ID: ' || v_empl.EMPLOYEE_ID || ' Nombre: ' || v_empl.FIRST_NAME || ' ' || v_empl.LAST_NAME); 
    END LOOP;                
END;

BEGIN
    FOR v_empl IN (SELECT *
                   FROM employees e
                   WHERE e.COMMISSION_PCT IS NOT NULL) LOOP
        DBMS_OUTPUT.PUT_LINE('ID: ' || v_empl.EMPLOYEE_ID || ' Nombre: ' || v_empl.FIRST_NAME || ' ' || v_empl.LAST_NAME); 
    END LOOP;                
END;

DECLARE
    --Declaraci�n
    CURSOR cur_employees (p_depto NUMBER) IS 
        SELECT *
        FROM employees e
        WHERE e.DEPARTMENT_ID = p_depto;
        
    v_empl employees%ROWTYPE;
    v_cant_reg NUMBER;
BEGIN
    OPEN cur_employees (50);
    
    LOOP
        FETCH cur_employees INTO v_empl;
        
        EXIT WHEN cur_employees%NOTFOUND;
        --EXIT WHEN cur_employees%ROWCOUNT > 10 OR cur_employees%NOTFOUND;
        
        DBMS_OUTPUT.PUT_LINE('ID: ' || v_empl.EMPLOYEE_ID || ' Nombre: ' || v_empl.FIRST_NAME || ' ' || v_empl.LAST_NAME); 
        
        v_cant_reg := cur_employees%ROWCOUNT;
        
        DBMS_OUTPUT.PUT_LINE('Registros Procesados: ' || v_cant_reg); 
    END LOOP;
    
    CLOSE cur_employees;
END;

DECLARE
    --Declaraci�n
    CURSOR cur_employees (p_job_id VARCHAR2, p_salary NUMBER) IS 
        SELECT *
        FROM employees e
        WHERE e.JOB_ID = p_job_id
        AND e.SALARY > p_salary
        ORDER BY e.FIRST_NAME ASC, e.LAST_NAME ASC;
        
    v_empl employees%ROWTYPE;
    v_cant_reg NUMBER;
BEGIN
    OPEN cur_employees ('IT_PROG', 5000);
    
    LOOP
        FETCH cur_employees INTO v_empl;
        
        EXIT WHEN cur_employees%NOTFOUND;
        --EXIT WHEN cur_employees%ROWCOUNT > 10 OR cur_employees%NOTFOUND;
        
        DBMS_OUTPUT.PUT_LINE('ID: ' || v_empl.EMPLOYEE_ID || ' Nombre: ' || v_empl.FIRST_NAME || ' ' || v_empl.LAST_NAME); 
        
        v_cant_reg := cur_employees%ROWCOUNT;
        
        DBMS_OUTPUT.PUT_LINE('Registros Procesados: ' || v_cant_reg); 
    END LOOP;
    
    CLOSE cur_employees;
END;

SELECT *
FROM EMPLOYEES;

DECLARE
    CURSOR cur_empl IS 
        SELECT *
        FROM EMPLOYEES_WITH_COMM
        --FOR UPDATE;
        FOR UPDATE OF salary NOWAIT;
BEGIN

    FOR v_empl IN cur_empl LOOP
        UPDATE EMPLOYEES_WITH_COMM
        SET salary = salary * 1.1
        WHERE CURRENT OF cur_empl;
    END LOOP;
    
    COMMIT;
END;

SELECT *
FROM EMPLOYEES;


DECLARE
    v_emp EMPLOYEES%ROWTYPE;
    v_error_code NUMBER;
    v_error_message VARCHAR2(4000);
BEGIN
    SELECT *
    INTO v_emp
    FROM EMPLOYEES e;
    --WHERE e.EMPLOYEE_ID = 1;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
      DBMS_OUTPUT.PUT_LINE('La consulta no retorna informaci�n'); 
    WHEN OTHERS THEN
      v_error_code := SQLCODE;
      v_error_message := SQLERRM;
      DBMS_OUTPUT.PUT_LINE('Se presento un error ' || v_error_code || ' * ' || v_error_message); 
END;

DECLARE
    v_emp EMPLOYEES%ROWTYPE;
    v_error_code NUMBER;
    v_error_message VARCHAR2(4000);
BEGIN
    SELECT *
    INTO v_emp
    FROM EMPLOYEES e;
    --WHERE e.EMPLOYEE_ID = 1;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
      DBMS_OUTPUT.PUT_LINE('La consulta no retorna informaci�n'); 
    WHEN  TOO_MANY_ROWS THEN
      DBMS_OUTPUT.PUT_LINE('La consulta retorna m�s de un registro'); 
    WHEN OTHERS THEN
      v_error_code := SQLCODE;
      v_error_message := SQLERRM;
      DBMS_OUTPUT.PUT_LINE('Se presento un error ' || v_error_code || ' * ' || v_error_message); 
END;

DECLARE
    v_emp EMPLOYEES%ROWTYPE;
    v_error_code NUMBER;
    v_error_message VARCHAR2(4000);
BEGIN
    SELECT *
    INTO v_emp
    FROM EMPLOYEES e
    WHERE e.EMPLOYEE_ID = 1;
EXCEPTION
    WHEN NO_DATA_FOUND OR TOO_MANY_ROWS THEN
      DBMS_OUTPUT.PUT_LINE('La consulta no es un cursor implicito'); 
    WHEN OTHERS THEN
      v_error_code := SQLCODE;
      v_error_message := SQLERRM;
      DBMS_OUTPUT.PUT_LINE('Se presento un error ' || v_error_code || ' * ' || v_error_message); 
END;

DECLARE
    EXC_INSERT_NULL EXCEPTION;
    PRAGMA EXCEPTION_INIT(EXC_INSERT_NULL, -01400);
    
    v_error_code NUMBER;
    v_error_message VARCHAR2(4000);
BEGIN
    INSERT INTO REGIONS (REGION_ID, REGION_NAME)
    VALUES(NULL, 'Region de prueba');
    
    COMMIT;
EXCEPTION
    WHEN EXC_INSERT_NULL THEN
        ROLLBACK;
    
        v_error_code := SQLCODE;
        v_error_message := SQLERRM;
        DBMS_OUTPUT.PUT_LINE('Intentando insertar un valor NULO ' || v_error_code || ' * ' || v_error_message); 
END;



DECLARE
    EXC_EMPLOYEE_NOT_FOUND EXCEPTION;
    v_emp EMPLOYEES%ROWTYPE;
    v_error_code NUMBER;
    v_error_message VARCHAR2(4000);
    v_id_emp EMPLOYEES.EMPLOYEE_ID%TYPE := 100;
    v_count_emp NUMBER := 0;
BEGIN
    SELECT COUNT(e.EMPLOYEE_ID)
    INTO v_count_emp
    FROM EMPLOYEES e
    WHERE e.EMPLOYEE_ID = v_id_emp;
    
    IF (v_count_emp <> 0) THEN
        SELECT *
        INTO v_emp
        FROM EMPLOYEES e
        WHERE e.EMPLOYEE_ID = v_id_emp;
    ELSE
        RAISE EXC_EMPLOYEE_NOT_FOUND;
    END IF;  
EXCEPTION
    WHEN EXC_EMPLOYEE_NOT_FOUND THEN
      v_error_code := SQLCODE;
      v_error_message := SQLERRM;
      
      DBMS_OUTPUT.PUT_LINE('El empleado con ID ' || v_id_emp || ' No existe' ); 
      DBMS_OUTPUT.PUT_LINE('Error: ' || v_error_code || ' * ' || v_error_message);
    WHEN OTHERS THEN
      v_error_code := SQLCODE;
      v_error_message := SQLERRM;
      DBMS_OUTPUT.PUT_LINE('Se presento un error ' || v_error_code || ' * ' || v_error_message); 
END;

DECLARE
    EXC_INSERT_NULL EXCEPTION;
    PRAGMA EXCEPTION_INIT(EXC_INSERT_NULL, -01400);
    
    v_error_code NUMBER;
    v_error_message VARCHAR2(4000);
BEGIN
    INSERT INTO REGIONS (REGION_ID, REGION_NAME)
    VALUES(NULL, 'Region de prueba');
    
    COMMIT;
EXCEPTION
    WHEN EXC_INSERT_NULL THEN
        ROLLBACK;
    
        v_error_code := SQLCODE;
        v_error_message := SQLERRM;
        DBMS_OUTPUT.PUT_LINE('Intentando insertar un valor NULO ' || v_error_code || ' * ' || v_error_message); 
        
        RAISE;
END;

DECLARE
    v_emp EMPLOYEES%ROWTYPE;
    v_error_code NUMBER;
    v_error_message VARCHAR2(4000);
    v_id_emp EMPLOYEES.EMPLOYEE_ID%TYPE := 1;
    v_count_emp NUMBER := 0;
BEGIN
    SELECT COUNT(e.EMPLOYEE_ID)
    INTO v_count_emp
    FROM EMPLOYEES e
    WHERE e.EMPLOYEE_ID = v_id_emp;
    
    IF (v_count_emp <> 0) THEN
        SELECT *
        INTO v_emp
        FROM EMPLOYEES e
        WHERE e.EMPLOYEE_ID = v_id_emp;
    ELSE
        raise_application_error(-20001, 'Empleado ' || v_id_emp || ' no existe');
    END IF;  
EXCEPTION
    WHEN OTHERS THEN
      v_error_code := SQLCODE;
      v_error_message := SQLERRM;
      DBMS_OUTPUT.PUT_LINE('Se presento un error ' || v_error_code || ' * ' || v_error_message); 
      
      RAISE;
END;

--Usando Excepciones
--Recibir la informaci�n para crear un empleado
-- Nombre y apellido
-- Fecha ingreso: Fecha actual
-- Nombre del cargo --> Si no existe, lanzar un -20001
-- Salario -> Salario se maypr que 0
-- Nombre del departamento --> Si no existe, lanzar un -20002
-- Si el empleado ya existe (nombre y apellido), debe lanzar una excepci�n EMPLOYEE_ALREADY_FOUND

create or replace NONEDITIONABLE PROCEDURE SP_CREATE_REGION (
    p_name IN REGIONS.REGION_NAME%TYPE,
    o_id IN OUT NUMBER
) IS
    V_CONTEO NUMBER := 0;
BEGIN

    SELECT COUNT(r.REGION_ID)
    INTO V_CONTEO
    FROM REGIONS r
    WHERE r.REGION_ID = o_id;

    IF (V_CONTEO > 0) THEN
        SELECT nvl(MAX(r.REGION_ID), 0) + 1
        INTO o_id
        FROM REGIONS r;
    END IF;

    INSERT INTO REGIONS (REGION_ID, REGION_NAME)
    VALUES (o_id, p_name);

    COMMIT;
END SP_CREATE_REGION;

DECLARE
    v_id_region REGIONS.REGION_ID%TYPE := 1;
BEGIN
    SP_CREATE_REGION('Asia Sur', v_id_region);
    DBMS_OUTPUT.PUT_LINE('La regi�n se creo con ID ' || v_id_region); 
END;

DECLARE
    v_id_region REGIONS.REGION_ID%TYPE := 1;
BEGIN
    SP_CREATE_REGION(o_id => v_id_region, p_name => 'Asia Norte');
    DBMS_OUTPUT.PUT_LINE('La regi�n se creo con ID ' || v_id_region); 
END;

SELECT *
FROM REGIONS;

create or replace PROCEDURE SP_CREATE_COUNTRY (
    p_id IN COUNTRIES.COUNTRY_ID%TYPE,
    p_name IN COUNTRIES.COUNTRY_NAME%TYPE,
    p_region_id IN COUNTRIES.REGION_ID%TYPE DEFAULT 5
) IS
    V_CONTEO NUMBER := 0;
BEGIN
    SELECT COUNT(c.COUNTRY_ID)
    INTO V_CONTEO
    FROM COUNTRIES c
    WHERE c.COUNTRY_ID = p_id;

    IF (V_CONTEO = 0) THEN
        INSERT INTO COUNTRIES (COUNTRY_ID, COUNTRY_NAME, REGION_ID)
        VALUES (p_id, p_name, p_region_id);
    ELSE
        raise_application_error(-20001, 'El pa�s ya existe');
    END IF;

    COMMIT;
EXCEPTION
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('Error ' || SQLCODE || ' - ' || SQLERRM);
        RAISE;
END SP_CREATE_COUNTRY;

BEGIN
    --SP_CREATE_COUNTRY('PE', 'Per�', 2);
    --SP_CREATE_COUNTRY('NZ', 'New Zeland');
    SP_CREATE_COUNTRY(p_id => 'NZ', p_name => 'New Zeland');
END;

SELECT *
FROM COUNTRIES;

DROP PROCEDURE SP_CREATE_COUNTRY;

SELECT *
FROM USER_SOURCE us
WHERE us.TYPE = 'PROCEDURE'
AND us.NAME = 'SP_CREATE_REGION'
ORDER BY us.LINE ASC;

--Crear un procedimiento SP_CREAR_EMPLEADO
-- Nombre completo (No haya Homonimo)
-- Email (Opcional)
-- Phone Number (Opcional)
-- Hire Date (Opcional, si no fecha actual)
-- Nombre del JobID (Busqueda y asignaci�n)
-- Salario (Verificar que sea mayor que 0)
-- Comision (Opcional)
-- Manager ID (Verificar si existe)
-- Nombre Departamento (Opcional, busqueda y asignaci�n)
--> Retornar el ID del empleado
--> Transaccionalidad

/*Reciba el ID de un empleado y retorne el historial de cargos del empleado iniciando desde el actual hasta el m�s antiguo. 
Se debe incluir la siguiente informaci�n:
Nombre y apellido del empleado
Fecha de inicio
Fecha de fin
Cargo al que pertenece
Departamento al que pertenece

NOTA: Si el empleado no existe, debe retornar el error.
*/

CREATE OR REPLACE FUNCTION FN_GET_AVERAGE_SALARY(
    p_id_depto DEPARTMENTS.DEPARTMENT_ID%TYPE
) RETURN NUMBER AS
    v_avg NUMBER := 0;
BEGIN
    SELECT NVL(AVG(e.SALARY), 0)
    INTO v_avg
    FROM EMPLOYEES e
    WHERE e.DEPARTMENT_ID = p_id_depto;
    
    RETURN v_avg;
END FN_GET_AVERAGE_SALARY;

DECLARE 
  v_avg NUMBER := 0;
BEGIN 
  v_avg := FN_GET_AVERAGE_SALARY(10);
  
  DBMS_OUTPUT.PUT_LINE('El promedio salarial es: ' || v_avg);
END;

DECLARE 
  v_avg NUMBER := 0;
BEGIN 
  SELECT FN_GET_AVERAGE_SALARY(10)
  INTO v_avg
  FROM dual;
   
  DBMS_OUTPUT.PUT_LINE('El promedio salarial es: ' || v_avg);
END;

BEGIN    
  DBMS_OUTPUT.PUT_LINE('El promedio salarial es: ' || FN_GET_AVERAGE_SALARY(10));
END;

EXECUTE DBMS_OUTPUT.PUT_LINE(FN_GET_AVERAGE_SALARY(10));
EXECUTE SP_CREATE_COUNTRY('PE', 'PERU');

SELECT AVG(e.salary)
FROM EMPLOYEES e
WHERE e.DEPARTMENT_ID = 500;

SELECT d.DEPARTMENT_ID, d.DEPARTMENT_NAME,
    FN_GET_AVERAGE_SALARY(d.DEPARTMENT_ID) AS "Salaria Promedio" 
FROM DEPARTMENTS d
ORDER BY DEPARTMENT_NAME ASC;

DROP FUNCTION FN_GET_AVERAGE_SALARY;

SELECT *
FROM USER_SOURCE us
WHERE us.TYPE = 'FUNCTION'
AND us.NAME = 'FN_GET_AVERAGE_SALARY'
ORDER BY us.LINE ASC;

--Ejemplos usando RETURNING
--RETURNING a Variables
DECLARE 
    v_depto_id DEPARTMENTS.DEPARTMENT_ID%TYPE;
    v_location_id DEPARTMENTS.LOCATION_ID%TYPE;
BEGIN
    INSERT INTO DEPARTMENTS(DEPARTMENT_ID, DEPARTMENT_NAME, MANAGER_ID,
        LOCATION_ID)  
    VALUES (DEPARTMENTS_SEQ.NEXTVAL, 'Departamento de prueba', 100, 
        (SELECT l.LOCATION_ID
         FROM LOCATIONS l
         WHERE l.LOCATION_ID = 1000))
    RETURNING DEPARTMENT_ID, LOCATION_ID 
    INTO v_depto_id, v_location_id;
    
    DBMS_OUTPUT.PUT_LINE('El ID es : ' || v_depto_id || ' Location: ' || v_location_id);
    
    ROLLBACK;
END;

--RETURNING a ROWTYPE
DECLARE 
    v_depto DEPARTMENTS%ROWTYPE;
BEGIN
    INSERT INTO DEPARTMENTS(DEPARTMENT_ID, DEPARTMENT_NAME, MANAGER_ID,
        LOCATION_ID)  
    VALUES (DEPARTMENTS_SEQ.NEXTVAL, 'Departamento de prueba', 100, 
        (SELECT l.LOCATION_ID
         FROM LOCATIONS l
         WHERE l.LOCATION_ID = 1000))
    RETURNING DEPARTMENT_ID, DEPARTMENT_NAME, MANAGER_ID, LOCATION_ID INTO v_depto;
    
    DBMS_OUTPUT.PUT_LINE('El ID es : ' || v_depto.DEPARTMENT_ID || ' Location: ' || v_depto.LOCATION_ID);
    
    ROLLBACK;
END;

--RETURNING a RECORD
DECLARE 
    TYPE t_depto IS RECORD(
        DEPARTMENT_ID DEPARTMENTS.DEPARTMENT_ID%TYPE,
        LOCATION_ID DEPARTMENTS.LOCATION_ID%TYPE
    );
    v_depto t_depto;
BEGIN
    INSERT INTO DEPARTMENTS(DEPARTMENT_ID, DEPARTMENT_NAME, MANAGER_ID,
        LOCATION_ID)  
    VALUES (DEPARTMENTS_SEQ.NEXTVAL, 'Departamento de prueba', 100, 
        (SELECT l.LOCATION_ID
         FROM LOCATIONS l
         WHERE l.LOCATION_ID = 1000))
    RETURNING DEPARTMENT_ID, LOCATION_ID INTO v_depto;
    
    DBMS_OUTPUT.PUT_LINE('El ID es : ' || v_depto.DEPARTMENT_ID || ' Location: ' || v_depto.LOCATION_ID);
    
    ROLLBACK;
END;

--RETURNING UPDATE 1 registro
DECLARE 
    TYPE t_depto IS RECORD(
        DEPARTMENT_ID DEPARTMENTS.DEPARTMENT_ID%TYPE,
        LOCATION_ID DEPARTMENTS.LOCATION_ID%TYPE
    );
    v_depto t_depto;
BEGIN
    UPDATE DEPARTMENTS
    SET DEPARTMENT_NAME = 'Administration'
    WHERE DEPARTMENT_ID = 10
    RETURNING DEPARTMENT_ID, LOCATION_ID INTO v_depto;
    
    DBMS_OUTPUT.PUT_LINE('El ID es : ' || v_depto.DEPARTMENT_ID || ' Location: ' || v_depto.LOCATION_ID);
    
    ROLLBACK;
END;

--RETURNING UPDATE * registro
DECLARE 
    TYPE t_depto IS TABLE OF DEPARTMENTS.DEPARTMENT_ID%TYPE;
    v_depto_list t_depto;
BEGIN
    UPDATE DEPARTMENTS
    SET DEPARTMENT_NAME = 'Administration'
    WHERE DEPARTMENT_ID >= 10
    RETURNING DEPARTMENT_ID BULK COLLECT INTO v_depto_list;
    
    FOR i IN v_depto_list.FIRST..v_depto_list.LAST LOOP
        DBMS_OUTPUT.PUT_LINE('El ID es : ' || v_depto_list(i));
    END LOOP;
    
    ROLLBACK;
END;

--RETURNING UPDATE * registro a ROWTYPE
DECLARE 
    TYPE t_depto IS TABLE OF DEPARTMENTS%ROWTYPE;
    v_depto_list t_depto;
BEGIN
    UPDATE DEPARTMENTS
    SET DEPARTMENT_NAME = 'Administration'
    WHERE DEPARTMENT_ID >= 10
    RETURNING DEPARTMENT_ID, DEPARTMENT_NAME, MANAGER_ID, LOCATION_ID BULK COLLECT INTO v_depto_list;
    
    FOR i IN v_depto_list.FIRST..v_depto_list.LAST LOOP
        DBMS_OUTPUT.PUT_LINE('El ID es : ' || v_depto_list(i).DEPARTMENT_ID || ': ' || v_depto_list(i).DEPARTMENT_NAME);
    END LOOP;
    
    ROLLBACK;
END;

--RETURNING UPDATE * registro a RECORD
DECLARE 
    TYPE t_depto IS RECORD(
        DEPARTMENT_ID DEPARTMENTS.DEPARTMENT_ID%TYPE,
        DEPARTMENT_NAME DEPARTMENTS.DEPARTMENT_NAME%TYPE
    );
    
    TYPE t_depto_list IS TABLE OF t_depto;
    v_depto_list t_depto_list;
BEGIN
    UPDATE DEPARTMENTS
    SET DEPARTMENT_NAME = 'Administration'
    WHERE DEPARTMENT_ID >= 10
    RETURNING DEPARTMENT_ID, DEPARTMENT_NAME BULK COLLECT INTO v_depto_list;
    
    FOR i IN v_depto_list.FIRST..v_depto_list.LAST LOOP
        DBMS_OUTPUT.PUT_LINE('El ID es : ' || v_depto_list(i).DEPARTMENT_ID || ': ' || v_depto_list(i).DEPARTMENT_NAME);
    END LOOP;
    
    ROLLBACK;
END;

--FORALL con TABLE (Anidada)
DECLARE 
    TYPE t_depto IS TABLE OF DEPARTMENTS.DEPARTMENT_ID%TYPE;
    v_depto_list t_depto;
BEGIN
    v_depto_list := t_depto(NULL);
    v_depto_list.EXTEND(3);
    v_depto_list(1) := 10;
    v_depto_list(2) := 20;
    v_depto_list(3) := 30;
    
    FORALL i IN v_depto_list.FIRST..v_depto_list.LAST
        UPDATE DEPARTMENTS
        SET DEPARTMENT_NAME = 'Administration'
        WHERE DEPARTMENT_ID = v_depto_list(i);
        
    DBMS_OUTPUT.PUT_LINE('El ID es : ' || SQL%ROWCOUNT); 
    
    ROLLBACK;
END;

--FORALL con VARRAY
DECLARE 
    TYPE t_depto IS VARRAY(3) OF DEPARTMENTS.DEPARTMENT_ID%TYPE;
    v_depto_list t_depto;
BEGIN
    v_depto_list := t_depto(10, 20, 30);
    
    FORALL i IN v_depto_list.FIRST..v_depto_list.LAST
        UPDATE DEPARTMENTS
        SET DEPARTMENT_NAME = 'Administration'
        WHERE DEPARTMENT_ID = v_depto_list(i);
        
    DBMS_OUTPUT.PUT_LINE('El ID es : ' || SQL%ROWCOUNT); 
    
    ROLLBACK;
END;

CREATE OR REPLACE PACKAGE PCK_EMPLOYEE_UTILS IS

--PUBLICO
--Constantes
  C_MY_CONSTANTE CONSTANT NUMBER := 25;
--Variables
  v_var1 VARCHAR2(500);
--Types, Cursores, Exceptions

--Funciones
  --Descripci�n
  -- Parametro de entrada
  -- Return
  -- Excepciones
  -- Track Chance (fecha, cambio, persona)
  FUNCTION FN_GET_MAX_SALARY
    RETURN NUMBER;
    
  FUNCTION FN_GET_MAX_SALARY(
    p_depto_id DEPARTMENTS.DEPARTMENT_ID%TYPE)
    RETURN NUMBER;

--Procedimientos
  PROCEDURE SP_EJECUTAR_NOMINA(
    p_username IN VARCHAR2(100)
  );

END PCK_EMPLOYEE_UTILS;

CREATE OR REPLACE PACKAGE BODY PCK_EMPLOYEE_UTILS IS

--PRIVADO
--Constantes
  C_MY_CONSTANTE_2 CONSTANT NUMBER := 50;
--Variables
  v_var2 VARCHAR2(500);
--Types, Cursores, Exceptions

--Funciones PRIVADAS
  FUNCTION FN_CALCULAR_EDAD(
    p_birth_date DATE
  ) RETURN NUMBER IS
  BEGIN
    RETURN 0;
  END;
  
--Procedimientos PRIVADOS
  PROCEDURE SP_CREAR_EMPLEADO(
    p_emp IN EMPLOYEES%ROWTYPE
  ) AS
  BEGIN
    NULL;
  END;

--Funciones PUBLICAS
  FUNCTION FN_GET_MAX_SALARY
    RETURN NUMBER IS
    BEGIN
        RETURN 0;
    END;

  FUNCTION FN_GET_MAX_SALARY(
    p_depto_id DEPARTMENTS.DEPARTMENT_ID%TYPE)
    RETURN NUMBER IS
    BEGIN
        RETURN 0;
    END;

--Procedimientos PUBLICAS
  PROCEDURE SP_EJECUTAR_NOMINA(
    p_username IN VARCHAR2
  ) IS
    v_edad NUMBER;
  BEGIN
    v_var1 := '1';
    v_var2 := 'Valor';
    v_edad := FN_CALCULAR_EDAD(SYSDATE);
    DBMS_OUTPUT.PUT_LINE('Constantes : ' || C_MY_CONSTANTE || ' C_MY_CONSTANTE_2 ' || C_MY_CONSTANTE_2); 
    DBMS_OUTPUT.PUT_LINE('El Usuario es : ' || p_username || ' edad ' || v_edad); 
  END;

END PCK_EMPLOYEE_UTILS;

-------------------------------------------

DECLARE
    v_salario NUMBER;
BEGIN
    --v_salario := PCK_EMPLOYEE_UTILS.FN_GET_MAX_SALARY;
    v_salario := PCK_EMPLOYEE_UTILS.FN_GET_MAX_SALARY(10);
    --PCK_EMPLOYEE_UTILS.SP_EJECUTAR_NOMINA(USER);
    --NO ES PUBLICO
    --v_salario := PCK_EMPLOYEE_UTILS.FN_CALCULAR_EDAD(sysdate);
    --PCK_EMPLOYEE_UTILS.SP_CREAR_EMPLEADO(null);
    
    
    --DBMS_OUTPUT.PUT_LINE('El Salario es : ' || v_salario); 
    --DBMS_OUTPUT.PUT_LINE('C_MY_CONSTANTE : ' || PCK_EMPLOYEE_UTILS.C_MY_CONSTANTE); 
    --DBMS_OUTPUT.PUT_LINE('v_var1 : ' || PCK_EMPLOYEE_UTILS.v_var1); 
END;

SELECT *
FROM USER_SOURCE us
WHERE us.TYPE = 'PACKAGE'
AND us.NAME = 'PCK_EMPLOYEE_UTILS'
ORDER BY us.LINE ASC;

SELECT *
FROM USER_SOURCE us
WHERE us.TYPE = 'PACKAGE BODY'
AND us.NAME = 'PCK_EMPLOYEE_UTILS'
ORDER BY us.LINE ASC;

DROP PACKAGE BODY PCK_EMPLOYEE_UTILS;
DROP PACKAGE PCK_EMPLOYEE_UTILS;

--UTL_FILE
CREATE DIRECTORY CACHAPARRO_DIR AS 'C:\temp\ORACLE_DIR';
GRANT READ, WRITE ON DIRECTORY CACHAPARRO_DIR TO HR;

DECLARE
    f_file_example UTL_FILE.FILE_TYPE;
    v_line VARCHAR2(4000);
BEGIN
    f_file_example := UTL_FILE.FOPEN('CACHAPARRO_DIR', 'FileExample_ConPipe.TXT', 'R');
    
    LOOP
        UTL_FILE.GET_LINE(f_file_example, v_line);
        
        DBMS_OUTPUT.PUT_LINE('Linea: ' || v_line); 
    END LOOP;
        
    UTL_FILE.FCLOSE(f_file_example);
EXCEPTION 
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('Error: ' || SQLCODE || ' - ' || SQLERRM); 
        
        IF (/*f_file_example IS NOT NULL AND*/UTL_FILE.IS_OPEN(f_file_example)) THEN
          UTL_FILE.FCLOSE(f_file_example);
        END IF;
END;

DECLARE
    f_file_example UTL_FILE.FILE_TYPE;
    v_line VARCHAR2(4000);
BEGIN
    f_file_example := UTL_FILE.FOPEN('CACHAPARRO_DIR', 'FileExample_ConPipe.TXT', 'R');
    
    BEGIN
        LOOP
            UTL_FILE.GET_LINE(f_file_example, v_line);
            
            DBMS_OUTPUT.PUT_LINE('Linea: ' || v_line); 
        END LOOP;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        DBMS_OUTPUT.PUT_LINE('Fin del archivo'); 
    END;
    
    UTL_FILE.FCLOSE(f_file_example);
EXCEPTION 
    WHEN UTL_FILE.INVALID_OPERATION THEN
        DBMS_OUTPUT.PUT_LINE('EL ARCHIVO NO EXISTE'); 
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('Error: ' || SQLCODE || ' - ' || SQLERRM); 
        
        IF (/*f_file_example IS NOT NULL AND*/UTL_FILE.IS_OPEN(f_file_example)) THEN
          UTL_FILE.FCLOSE(f_file_example);
        END IF;
END;

DECLARE 
    f_file_example UTL_FILE.FILE_TYPE;
BEGIN
    f_file_example := UTL_FILE.FOPEN('CACHAPARRO_DIR', 'Employees.TXT', 'W');
    
    UTL_FILE.NEW_LINE(f_file_example);
    
    FOR v_emp IN (SELECT *
                  FROM EMPLOYEES e) LOOP
        DBMS_OUTPUT.PUT_LINE('Escribiendo empleado: ' || v_emp.EMPLOYEE_ID || ' - ' || v_emp.FIRST_NAME); 
        
        UTL_FILE.PUT_LINE(f_file_example, v_emp.EMPLOYEE_ID || ';' || v_emp.FIRST_NAME || ';' || v_emp.LAST_NAME);
    END LOOP;
    
    UTL_FILE.FCLOSE(f_file_example);  
EXCEPTION 
    WHEN UTL_FILE.WRITE_ERROR THEN
         DBMS_OUTPUT.PUT_LINE('Error de escritura: ' || SQLCODE || ' - ' || SQLERRM);
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('Error: ' || SQLCODE || ' - ' || SQLERRM); 
        
        IF (/*f_file_example IS NOT NULL AND*/UTL_FILE.IS_OPEN(f_file_example)) THEN
          UTL_FILE.FCLOSE(f_file_example);
        END IF;
END;


--Procedimiento almacenado
-- IN: Separador
-- IN: Nombre del archivo
-- IN: Directorio
--Nombre completo
--Email
--Fecha de ingreso (DD/MM/AAAA)
--Nombre del cargo
--Salario
--Comisi�n (Si no existe 0)
--Nombre del departamento

SELECT *
FROM EMPLOYEES;


BEGIN
    EXECUTE IMMEDIATE 'CREATE TABLE TEST_1(ID NUMBER NOT NULL, NAME VARCHAR2(500) NOT NULL)';
END;

DECLARE
    v_sql VARCHAR2(4000);
BEGIN
    v_sql := 'CREATE TABLE TEST_2(ID NUMBER NOT NULL, NAME VARCHAR2(500) NOT NULL)';

    EXECUTE IMMEDIATE v_sql;
END;

CREATE OR REPLACE PROCEDURE SP_INSERTAR_DYNAMIC(
    p_table IN VARCHAR2,
    p_id IN NUMBER,
    p_name IN VARCHAR2
) AS
    v_sql VARCHAR2(4000);
BEGIN
    v_sql := 'INSERT INTO ' || p_table || '(ID, NAME) VALUES (' || p_id || ',''' || p_name || ''')';
    DBMS_OUTPUT.PUT_LINE('SQL: ' || v_sql);
    
    EXECUTE IMMEDIATE v_sql;
    
    COMMIT;
END;

CREATE OR REPLACE PROCEDURE SP_INSERTAR_DYNAMIC(
    p_table IN VARCHAR2,
    p_id IN NUMBER,
    p_name IN VARCHAR2
) AS
    v_sql VARCHAR2(4000);
BEGIN
    v_sql := 'INSERT INTO ' || p_table || '(ID, NAME) VALUES (:1, :2)';
    DBMS_OUTPUT.PUT_LINE('SQL: ' || v_sql);
    
    EXECUTE IMMEDIATE v_sql USING p_id, p_name;
    
    DBMS_OUTPUT.PUT_LINE('Cantidad de registros insertados: ' || SQL%ROWCOUNT);
    
    COMMIT;
END;

BEGIN
    SP_INSERTAR_DYNAMIC('TEST_2', 1, 'PEDRO');
END;

ROLLBACK;

SELECT *
FROM TEST_2;



CREATE OR REPLACE PROCEDURE SP_LEER_DYNAMIC(
    p_table IN VARCHAR2,
    p_id IN NUMBER
) AS
    v_sql VARCHAR2(4000);
    v_id NUMBER;
    v_name VARCHAR2(500);
BEGIN
    v_sql := 'SELECT * FROM ' || p_table || ' WHERE ID = :1';
    DBMS_OUTPUT.PUT_LINE('SQL: ' || v_sql);
    
    EXECUTE IMMEDIATE v_sql 
    INTO v_id, v_name
    USING p_id;
    
    DBMS_OUTPUT.PUT_LINE('Registro con ID: ' || v_id || ': ' || v_name);
END;

CREATE OR REPLACE PROCEDURE SP_LEER_DYNAMIC(
    p_table IN VARCHAR2,
    p_id IN NUMBER
) AS
    v_sql VARCHAR2(4000);
    v_data TEST_1%ROWTYPE;
BEGIN
    v_sql := 'SELECT * FROM ' || p_table || ' WHERE ID = :1';
    DBMS_OUTPUT.PUT_LINE('SQL: ' || v_sql);
    
    EXECUTE IMMEDIATE v_sql 
    INTO v_data
    USING p_id;
    
    DBMS_OUTPUT.PUT_LINE('Registro con ID: ' || v_data.ID || ': ' || v_data.name);
END;


begin
    SP_LEER_DYNAMIC('TEST_1', 1);
end;

--Crear un procedimiento usando NSQL para
--1. Consultar la informaci�n del empleado por ID, debe recibir el ID y retornar la informaci�n del empleado.
--2. Crear o actualizar un Pais (COUNTRY), ID, Nombre del pa�s y el nombre de la regi�n, si el pais existe lo actualiza, sino lo crea

CREATE OR REPLACE PROCEDURE SP_LEER_DYNAMIC(
    p_table IN VARCHAR2,
    p_id IN NUMBER
) AS
    v_sql VARCHAR2(4000);
    v_data TEST_1%ROWTYPE;
BEGIN
    v_sql := 'SELECT * FROM ' || p_table;
    DBMS_OUTPUT.PUT_LINE('SQL: ' || v_sql);
    
    EXECUTE IMMEDIATE v_sql 
    INTO v_data;
    --USING p_id;
    
    DBMS_OUTPUT.PUT_LINE('Registro con ID: ' || v_data.ID || ': ' || v_data.name);
EXCEPTION
    WHEN NO_DATA_FOUND OR TOO_MANY_ROWS THEN
        DBMS_OUTPUT.PUT_LINE('Se presento un error'); 
END;

begin
    SP_LEER_DYNAMIC('TEST_1', 100);
end;

DECLARE
    v_sql VARCHAR2(4000);
    v_cursor NUMBER;
    v_res NUMBER;
BEGIN
    v_sql := 'CREATE TABLE TEST_3(ID NUMBER NOT NULL, NAME VARCHAR2(500) NOT NULL)';
    v_cursor := DBMS_SQL.OPEN_CURSOR;
    
    DBMS_OUTPUT.PUT_LINE('Open Cursor: ' || v_cursor ); 
    
    DBMS_SQL.PARSE(v_cursor, v_sql, DBMS_SQL.NATIVE);  
    
    v_res := DBMS_SQL.EXECUTE(v_cursor);
    
    DBMS_OUTPUT.PUT_LINE('Resultado: ' || v_res ); 
    
    DBMS_SQL.CLOSE_CURSOR(v_cursor);
END;

DECLARE
    v_sql VARCHAR2(4000);
    v_cursor NUMBER;
    v_res NUMBER;
BEGIN
    v_sql := 'CREATE TABLE TEST_3(ID NUMBER NOT NULL, NAME VARCHAR2(500) NOT NULL)';
    v_cursor := DBMS_SQL.OPEN_CURSOR;
    
    DBMS_OUTPUT.PUT_LINE('Open Cursor: ' || v_cursor ); 
    
    DBMS_SQL.PARSE(v_cursor, v_sql, DBMS_SQL.NATIVE);  
    
    v_res := DBMS_SQL.EXECUTE(v_cursor);
    
    DBMS_OUTPUT.PUT_LINE('Resultado: ' || v_res ); 
    
    DBMS_SQL.CLOSE_CURSOR(v_cursor);
END;

CREATE OR REPLACE PROCEDURE SP_INSERTAR_DYNAMIC(
    p_table IN VARCHAR2,
    p_id IN NUMBER,
    p_name IN VARCHAR2
) AS
    v_sql VARCHAR2(4000);
    v_cursor NUMBER;
    v_res NUMBER;
BEGIN
    v_sql := 'INSERT INTO ' || p_table || '(ID, NAME) VALUES (:1, :2)';
    DBMS_OUTPUT.PUT_LINE('SQL: ' || v_sql);
    
    v_cursor := DBMS_SQL.OPEN_CURSOR;
    
    DBMS_SQL.PARSE(v_cursor, v_sql, DBMS_SQL.NATIVE);
    
    DBMS_SQL.BIND_VARIABLE(v_cursor, ':1', p_id);
    DBMS_SQL.BIND_VARIABLE(v_cursor, ':2', p_name);
    
    v_res := DBMS_SQL.EXECUTE(v_cursor);
    
    DBMS_OUTPUT.PUT_LINE('Cantidad de registros insertados: ' || v_res);
    
    DBMS_SQL.CLOSE_CURSOR(v_cursor);
    
    COMMIT;
END;

BEGIN
    SP_INSERTAR_DYNAMIC('TEST_3', 1, 'RAUL');
END;

SELECT *
FROM TEST_3;

create or replace NONEDITIONABLE PROCEDURE SP_LEER_DYNAMIC(
    p_table IN VARCHAR2,
    p_id IN NUMBER
) AS
    v_sql VARCHAR2(4000);
    v_data TEST_1%ROWTYPE;
    v_cursor NUMBER;
    v_res NUMBER;
BEGIN
    v_sql := 'SELECT * FROM ' || p_table || ' WHERE ID = :1';
    DBMS_OUTPUT.PUT_LINE('SQL: ' || v_sql);

    v_cursor := DBMS_SQL.OPEN_CURSOR;

    DBMS_SQL.PARSE(v_cursor, v_sql, DBMS_SQL.NATIVE);

    DBMS_SQL.BIND_VARIABLE(v_cursor, ':1', p_id);

    DBMS_SQL.DEFINE_COLUMN(v_cursor, 1, v_data.ID);
    --DBMS_SQL.DEFINE_COLUMN(v_cursor, 2, v_data.NAME);
    DBMS_SQL.DEFINE_COLUMN(v_cursor, 2, v_data.NAME, 500);

    v_res := DBMS_SQL.EXECUTE(v_cursor);

    DBMS_OUTPUT.PUT_LINE('Cantidad de registros consultados: ' || v_res);

    v_res := DBMS_SQL.FETCH_ROWS(v_cursor);

    DBMS_SQL.COLUMN_VALUE(v_cursor, 1, v_data.ID);
    DBMS_SQL.COLUMN_VALUE(v_cursor, 2, v_data.NAME);

    DBMS_OUTPUT.PUT_LINE('Registro con ID: ' || v_data.ID || ': ' || v_data.name);

    DBMS_SQL.CLOSE_CURSOR(v_cursor);
EXCEPTION 
    WHEN OTHERS THEN
        IF (DBMS_SQL.IS_OPEN(v_cursor)) THEN 
            DBMS_SQL.CLOSE_CURSOR(v_cursor);
        END IF;
END;


begin
    SP_LEER_DYNAMIC('TEST_3', 1);
end;

CREATE OR REPLACE PROCEDURE SP_LEER_DYNAMIC(
    p_table IN VARCHAR2,
    p_id IN NUMBER
) AS
    v_sql VARCHAR2(4000);
    v_data TEST_1%ROWTYPE;
    v_cursor NUMBER;
    v_res NUMBER;
BEGIN
    v_sql := 'SELECT * FROM ' || p_table || ' WHERE ID = :1';
    DBMS_OUTPUT.PUT_LINE('SQL: ' || v_sql);
    
    v_cursor := DBMS_SQL.OPEN_CURSOR;
    
    DBMS_SQL.PARSE(v_cursor, v_sql, DBMS_SQL.NATIVE);
    
    DBMS_SQL.BIND_VARIABLE(v_cursor, ':1', p_id);
    DBMS_SQL.DEFINE_COLUMN(v_cursor, 1, v_data.ID);
    DBMS_SQL.DEFINE_COLUMN(v_cursor, 2, v_data.NAME, 500);
    
    v_res := DBMS_SQL.EXECUTE(v_cursor);
    
    DBMS_OUTPUT.PUT_LINE('Cantidad de registros consultados: ' || v_res);
    
    LOOP
        v_res := DBMS_SQL.FETCH_ROWS(v_cursor);
        DBMS_OUTPUT.PUT_LINE('v_res: ' || v_res);
        
        EXIT WHEN v_res = 0;
        
        DBMS_SQL.COLUMN_VALUE(v_cursor, 1, v_data.ID);
        DBMS_SQL.COLUMN_VALUE(v_cursor, 2, v_data.NAME);
        
        DBMS_OUTPUT.PUT_LINE('Registro con ID: ' || v_data.ID || ': ' || v_data.name);
    END LOOP;
        
    DBMS_SQL.CLOSE_CURSOR(v_cursor);
EXCEPTION 
    WHEN OTHERS THEN
        IF (DBMS_SQL.IS_OPEN(v_cursor)) THEN 
            DBMS_SQL.CLOSE_CURSOR(v_cursor);
        END IF;
END;

begin
    SP_LEER_DYNAMIC('TEST_1', 1);
end;

select *
from test_1;

CREATE OR REPLACE PROCEDURE SP_LEER_DYNAMIC(
    p_table IN VARCHAR2,
    p_id IN NUMBER
) AS
    v_sql VARCHAR2(4000);
    v_data TEST_1%ROWTYPE;
    v_cursor NUMBER;
    v_res NUMBER;
BEGIN
    v_sql := 'SELECT * FROM ' || p_table || ' WHERE ID = :1';
    DBMS_OUTPUT.PUT_LINE('SQL: ' || v_sql);
    
    v_cursor := DBMS_SQL.OPEN_CURSOR;
    
    DBMS_SQL.PARSE(v_cursor, v_sql, DBMS_SQL.NATIVE);
        
    FOR v_cont IN 1..5 LOOP 
        DBMS_SQL.BIND_VARIABLE(v_cursor, ':' || v_cont, list_ids(v_cont));
    END LOOP;
    
    DBMS_SQL.DEFINE_COLUMN(v_cursor, 1, v_data.ID);
    DBMS_SQL.DEFINE_COLUMN(v_cursor, 2, v_data.NAME, 500);
    
    v_res := DBMS_SQL.EXECUTE(v_cursor);
    
    DBMS_OUTPUT.PUT_LINE('Cantidad de registros consultados: ' || v_res);
    
    LOOP
        v_res := DBMS_SQL.FETCH_ROWS(v_cursor);
        DBMS_OUTPUT.PUT_LINE('v_res: ' || v_res);
        
        EXIT WHEN v_res = 0;
        
        DBMS_SQL.COLUMN_VALUE(v_cursor, 1, v_data.ID);
        DBMS_SQL.COLUMN_VALUE(v_cursor, 2, v_data.NAME);
        
        DBMS_OUTPUT.PUT_LINE('Registro con ID: ' || v_data.ID || ': ' || v_data.name);
    END LOOP;
        
    DBMS_SQL.CLOSE_CURSOR(v_cursor);
EXCEPTION 
    WHEN OTHERS THEN
        IF (DBMS_SQL.IS_OPEN(v_cursor)) THEN 
            DBMS_SQL.CLOSE_CURSOR(v_cursor);
        END IF;
END;







 








 



